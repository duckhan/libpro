#include <QString>
#include "test_setup.h"
#include "../LibProLib/Common/filehelper.h"
class FileHelperTest : public QObject
{
    Q_OBJECT

public:
    FileHelperTest();

private Q_SLOTS:
    void ReadAllLineTest();
    void SaveAllLineTest();
};

FileHelperTest::FileHelperTest()
{

}

void FileHelperTest::ReadAllLineTest()
{
    QString filePath("D:\\KTLT\\Data\\sample.txt");
    QStringList result=FileHelper::ReadAllLine(filePath);
    QString actual=result.at(0);
    QString expect("duc khan");
    QVERIFY(expect==actual);
    QVERIFY(result.length()==4);
}

void FileHelperTest::SaveAllLineTest()
{
     QString filePath("D:\\KTLT\\Data\\out.txt");
     QStringList lines;
     lines << "Hello" << "Duc Khan";
     bool ret=FileHelper::SaveAllLine(filePath,lines);
     QVERIFY(ret);
}


#ifdef FileHelperTestExcute
QTEST_MAIN(FileHelperTest);
#endif

#include "tst_filehelpertest.moc"
