#ifndef FILEHELPER_H
#define FILEHELPER_H
#include <QFile>
#include <QList>
#include <QString>
class FileHelper
{
public:

    FileHelper();
    static QStringList ReadAllLine(QString filePath);
    static bool SaveAllLine(QString filePath,QStringList lines);
};

#endif // FILEHELPER_H
