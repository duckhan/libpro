#ifndef MESSAGE_H
#define MESSAGE_H
#include <QString>
#include <QTextStream>
#include <QDateTime>
class Message
{
private:
    int _SendID;
    int _Received;
    QString _Content;
    QDateTime _sentDate;
public:
    Message();
    Message(int id, int rec, QString cont);
    Message(QString data);
    int getSendID();
    void setSendID(int val);
    int getReceived();
    void setReceived(int val);
    QString getContent();
    void setContent(QString val);
    QDateTime getSentDate();
    void setSentDate(QDateTime val);
    QString toString();
};

#endif // MESSAGE_H
