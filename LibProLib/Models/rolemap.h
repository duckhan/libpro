#ifndef ROLEMAP_H
#define ROLEMAP_H
#include <QString>
#include <QTextStream>


class RoleMap
{
private:
    int _AccountID;
    int _RoleID;
public:
    RoleMap();
    RoleMap(int AcID, int RoID);
    RoleMap(QString data);
    int getAccountID();
    void setAccountID(int val);
    int getRoleID();
    void setRoleID(int val);
    QString toString();
};

#endif // ROLEMAP_H
