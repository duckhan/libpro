#include "session.h"


Session::Session(Account* acc, RoleMap* role)
{
    _account=acc;
    _role=role;
}

Account* Session::getAccount()
{
    return _account;
}

RoleMap* Session::getRoleMap()
{
    return _role;
}
