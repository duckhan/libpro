#ifndef SESSION_H
#define SESSION_H
#include "../Models/account.h"
#include "../Models/rolemap.h"
class Session
{
private:
    Account* _account;
    RoleMap* _role;
public:
    Session();
    Session(Account* acc,RoleMap* role);
    Account* getAccount();
    RoleMap* getRoleMap();
};

#endif // SESSION_H
