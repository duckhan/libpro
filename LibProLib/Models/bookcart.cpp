#include "bookcart.h"

BookCart::BookCart()
{

}

BookCart::BookCart(int id, int uid, bool stt)
{
    _CartID=id;
    _UserID=uid;
    _Status=stt;
}

BookCart::BookCart(QString data)
{
    QStringList values=data.split('|');
    _CartID=((QString)values.at(0)).toInt();
    _UserID=((QString)values.at(1)).toInt();
    _Status=((QString)values.at(2)).toInt();
}

int BookCart::getCartID()
{
    return _CartID;
}

void BookCart::setCartID(int val)
{
    _CartID=val;
}

int BookCart::getUserID()
{
    return _UserID;
}

void BookCart::setUserID(int val)
{
    _UserID=val;
}

bool BookCart::getStatus()
{
    return _Status;
}

void BookCart::setStatus(bool val)
{
    _Status=val;
}

QString BookCart::toString()
{
    QString result;
    QTextStream ts(&result);
    ts << _CartID <<'|' <<_UserID<< '|' << _Status;
    return result;
}
