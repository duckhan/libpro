#ifndef BOOKCART_H
#define BOOKCART_H
#include <QString>
#include <QTextStream>


class BookCart
{
private:
    int _CartID;
    int _UserID;
    bool _Status;
public:
    BookCart();
    BookCart(int id, int uid, bool stt);
    BookCart(QString data);
    int getCartID();
    void setCartID(int val);
    int getUserID();
    void setUserID(int val);
    bool getStatus();
    void setStatus(bool val);
    QString toString();

};

#endif // BOOKCART_H
