#ifndef REPOSITORY_H
#define REPOSITORY_H
#include <QString>
#include <QTextStream>

class BookDetail
{
private:
    int _BookID;
    int _Quantity;
    QString _Position;
    int _BorrowedCount;
public:
    BookDetail();
    BookDetail(int book, int quan, QString pos, int BoCount);
    BookDetail(QString data);
    int getBookID();
    void setBookID(int val);
    int getQuantity();
    void setQuantity(int val);
    QString getPosition();
    void setPosition(QString val);
    int getBorrowedCount();
    void setBorrowedCount(int val);
    QString toString();
};

#endif // REPOSITORY_H
