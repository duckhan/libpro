#include "borrowedbook.h"

BorrowedBook::BorrowedBook()
{

}

BorrowedBook::BorrowedBook(int BorID, int BoID, int UsID, QDateTime ExDa)
{
    _BorrowedID=BorID;
    _BookID=BoID;
    _UserID=UsID;
    _ExpiryDate=ExDa;

}

BorrowedBook::BorrowedBook(QString data)
{
    QStringList values=data.split('|');
    _BorrowedID=((QString)values.at(0)).toInt();
    _BookID=((QString)values.at(1)).toInt();
    _UserID=((QString)values.at(2)).toInt();
   _ExpiryDate=QDateTime::fromString((QString)values.at(3),Qt::ISODate);

}

int BorrowedBook::getBorrowedID()
{
    return _BorrowedID;
}

void BorrowedBook::setBorrowedID(int val)
{
       _BorrowedID=val;
}
int BorrowedBook::getBookID()
{
    return _BookID;
}
void BorrowedBook::setBookID(int val)
{
    _BookID=val;
}


int BorrowedBook::getUserID()
{
    return _UserID;
}
void BorrowedBook::setUserID(int val)
{
    _UserID=val;
}

QDateTime BorrowedBook::getExpiryDate()
{
    return _ExpiryDate;
}

void BorrowedBook::setExpiryDate(QDateTime val)
{
    _ExpiryDate=val;
}

QString BorrowedBook::toString()
{
    QString result;
    QTextStream ts(&result);
    ts << _BorrowedID <<'|' <<_BookID << '|' << _UserID <<'|'<<_ExpiryDate.toString(Qt::ISODate);
    return result;
}
