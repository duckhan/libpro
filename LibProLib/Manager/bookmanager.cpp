#include "bookmanager.h"
#include "../LibProLib/Common/filehelper.h"
BookManager BookManager::_instance=BookManager();
void BookManager::loadBooks(QString file)
{
    QStringList lines=FileHelper::ReadAllLine(file);
    for(QString line: lines)
    {
        try
        {
            Book* u=new Book(line);
            _books.append(u);
        }
        catch(...)
        {
            continue;
        }
    }
}

void BookManager::loadBorrowedBooks(QString file)
{

}



bool BookManager::saveBooks(QString file)
{

    return false;
}

bool BookManager::saveBorrowedBook(QString file)
{
    return false;

}



BookManager::BookManager()
{
    _bookFile="D:\\KTLT\\Data\\Book.txt";
    loadBooks(_bookFile);
}

BookManager::~BookManager()
{

}

QList<Book *> BookManager::Books()
{
 return _books;
}

Book *BookManager::getBookById(int bookId)
{
      for(Book* b:_books)
      {
          if(b->getBookID()==bookId)
              return b;
      }
      return nullptr;
}

QList<Book *> BookManager::getBooksByTitle(QString name)
{
    QList<Book*> result;
    for(Book* book:_books)
    {
        if(book->getTitle().contains(name,Qt::CaseInsensitive))
        {
            result.append(book);
        }
    }
    return result;
}



void BookManager::addBook(Book &book)
{

}

void BookManager::modifyBook(Book &book)
{

}

void BookManager::removeBook(int bookId)
{

}

void BookManager::addBorrowedBook(BorrowedBook &book)
{

}

void BookManager::modifyBorrowedBook(BorrowedBook &book)
{

}

void BookManager::removeBorrowedBook(int id)
{

}

BookManager &BookManager::getInstance()
{
    return _instance;
}
