#include "sessionmanager.h"
Session* SessionManager::_session=nullptr;
SessionManager::SessionManager()
{

}

Session* SessionManager::Current()
{
    return _session;
}

void SessionManager::SetSession(Session *sec)
{
    _session=sec;
}

SessionManager::~SessionManager()
{
    if(_session==nullptr)
        return;
    delete _session;
    _session=nullptr;
}
