#ifndef BOOKMANAGER_H
#define BOOKMANAGER_H
#include "../Models/book.h"
#include "../Models/borrowedbook.h"
#include "../Models/bookdetail.h"
#include <QList>
class BookManager
{
private:
QList<Book*> _books;
QList<BorrowedBook*> _borrowedBooks;
QList<BookDetail*> _bookDetails;
QString _bookFile;
QString _borrowedBookFile;

void loadBooks(QString file);
void loadBorrowedBooks(QString file);

bool saveBooks(QString file);
bool saveBorrowedBook(QString file);

BookManager();
BookManager(BookManager const&);
void operator=(BookManager const &);
static BookManager _instance;
public:
    ~BookManager();
QList<Book*> Books();
    Book* getBookById(int bookId);
    QList<Book *> getBooksByTitle(QString name);
    void addBook(Book& book);
    void modifyBook(Book& book);
    void removeBook(int bookId);
    BorrowedBook *getBorrowedBookByid(int id);
    void addBorrowedBook(BorrowedBook& book);
    void modifyBorrowedBook(BorrowedBook& book);
    void removeBorrowedBook(int id);
    static BookManager& getInstance();
};

#endif // BOOKMANAGER_H
