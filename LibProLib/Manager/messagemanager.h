#ifndef MESSAGEMANAGER_H
#define MESSAGEMANAGER_H

#include "../Models/message.h"
#include "../Models/user.h"
#include <QList>
class MessageManager
{
private:
    QList<Message*> _message;
    QString _messageFile;
    void loadMessage(QString file);
    bool _isDirty;
    void saveMessage(QString file);
    MessageManager _instance;
    MessageManager(MessageManager const&);
    MessageManager();
    void operator =(MessageManager const&);
public:
    ~MessageManager();
    MessageManager& getInstance();
    QList<Message*> getMessageByUser(int userid);
    void addMessage(Message& m);
    void removeMessage(int messId);

};

#endif // MESSAGEMANAGER_H
