#ifndef SESSIONMANAGER_H
#define SESSIONMANAGER_H
#include "../Models/session.h"

class SessionManager
{
private:
    static Session* _session;
public:
    SessionManager();
    static Session* Current();
    static void SetSession(Session* sec);
    ~SessionManager();
};

#endif // SESSIONMANAGER_H
