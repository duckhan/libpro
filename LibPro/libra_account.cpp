#include "libra_account.h"
#include "ui_libra_account.h"

Libra_account::Libra_account(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Libra_account)
{
    ui->setupUi(this);
}

Libra_account::~Libra_account()
{
    delete ui;
}
