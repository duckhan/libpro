#ifndef MODIFY_BOOK_H
#define MODIFY_BOOK_H

#include <QWidget>

namespace Ui {
class Modify_book;
}

class Modify_book : public QWidget
{
    Q_OBJECT

public:
    explicit Modify_book(QWidget *parent = 0);
    ~Modify_book();

private:
    Ui::Modify_book *ui;
};

#endif // MODIFY_BOOK_H
