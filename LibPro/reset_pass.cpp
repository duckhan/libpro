#include "reset_pass.h"
#include "ui_reset_pass.h"
#include "../LibProLib/Manager/sessionmanager.h"
#include "../LibProLib/Manager/usermanager.h"
#include "navigationservice.h"
#include <QMessageBox>
Reset_pass::Reset_pass(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Reset_pass)
{
    ui->setupUi(this);
}

Reset_pass::~Reset_pass()
{
    delete ui;
}

void Reset_pass::on_ResetButton_clicked()
{
    QString old=ui->OldPasswordlineEdit->text();
    QString newPass=ui->NewPasswordlineEdit->text();
    Account* acc=SessionManager::Current()->getAccount();
    if(acc->getPassword()!=old)
    {
        QMessageBox::warning(this,"Warning","Sai mật khẩu");
    }
    else
    {
        acc->setPassword(newPass);
        UserManager::Instance().ModifyAccount(*acc);
        QMessageBox::information(this,"Infomation","Đổi mật khẩu thành công");
        NavigationService::getInstance().goBack();
    }

}
