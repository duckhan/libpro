#ifndef ADMIN_PROFILE_H
#define ADMIN_PROFILE_H

#include <QWidget>

namespace Ui {
class Admin_profile;
}

class Admin_profile : public QWidget
{
    Q_OBJECT

public:
    explicit Admin_profile(QWidget *parent = 0);
    ~Admin_profile();

private:
    Ui::Admin_profile *ui;
};

#endif // ADMIN_PROFILE_H
