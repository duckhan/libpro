#ifndef UNLOCK_ACCOUNT_H
#define UNLOCK_ACCOUNT_H

#include <QWidget>

namespace Ui {
class Unlock_account;
}

class Unlock_account : public QWidget
{
    Q_OBJECT

public:
    explicit Unlock_account(QWidget *parent = 0);
    ~Unlock_account();

private:
    Ui::Unlock_account *ui;
};

#endif // UNLOCK_ACCOUNT_H
