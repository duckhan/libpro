#include "navigationservice.h"
#include "admin_profile.h"
#include "user_profile.h"
#include "libra_account.h"
#include "search_book.h"
#include "notice.h"
NavigationService NavigationService::_instance=NavigationService();
NavigationService::NavigationService()
{
    _current=nullptr;
}

void NavigationService::init(QWidget *mainFrame)
{
    _instance._mainFrame=mainFrame;
}

NavigationService &NavigationService::getInstance()
{
    if(_instance._mainFrame==nullptr)
    {
        throw "run init before use it";
    }
    return _instance;
}

void NavigationService::navigate(eWidget e)
{
    if(_current!=nullptr)
    {
        _frames.append(_current);
    }
    switch (e)
    {
    case eWidget::WNotice:
    {
        _current=new Notice(_mainFrame);
        break;
    }
    case eWidget::WMain_menu:
    {
        _current=new Main_menu(_mainFrame);
        break;
    }
    case eWidget::WLogin:
    {
        _current=new Login(_mainFrame);
        break;
    }
    case eWidget::WSearch_book:
    {
        _current=new Search_book(_mainFrame);
       break;
    }
    case eWidget::WAdmin_profile:
    {

         _current=new Admin_profile(_mainFrame);
        break;

    }
    case eWidget::WUser_profile:
    {
         _current=new User_profile(_mainFrame);
        break;
    }
    case eWidget::WLibra_account:
    {
         _current=new Libra_account(_mainFrame);
        break;
    }
    default:
        _current=new Main_menu(_mainFrame);
        break;
    }
    _current->show();
}

void NavigationService::goBack()
{
    _current->hide();
    _waitDelete.append(_current);

   _current= _frames.pop();
   _current->show();

}

void NavigationService::goHome()
{
    _current->hide();
    _waitDelete.append(_current);
    _current=_frames.top();
    _frames.clear();
    _current->show();
}
