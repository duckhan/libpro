#ifndef TAKEN_BOOK_H
#define TAKEN_BOOK_H

#include <QWidget>

namespace Ui {
class Taken_book;
}

class Taken_book : public QWidget
{
    Q_OBJECT

public:
    explicit Taken_book(QWidget *parent = 0);
    ~Taken_book();

private:
    Ui::Taken_book *ui;
};

#endif // TAKEN_BOOK_H
