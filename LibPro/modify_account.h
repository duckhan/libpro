#ifndef MODIFY_ACCOUNT_H
#define MODIFY_ACCOUNT_H

#include <QWidget>

namespace Ui {
class Modify_account;
}

class Modify_account : public QWidget
{
    Q_OBJECT

public:
    explicit Modify_account(QWidget *parent = 0);
    ~Modify_account();

private:
    Ui::Modify_account *ui;
};

#endif // MODIFY_ACCOUNT_H
