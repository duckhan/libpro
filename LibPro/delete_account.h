#ifndef DELETE_ACCOUNT_H
#define DELETE_ACCOUNT_H

#include <QWidget>

namespace Ui {
class Delete_account;
}

class Delete_account : public QWidget
{
    Q_OBJECT

public:
    explicit Delete_account(QWidget *parent = 0);
    ~Delete_account();

private:
    Ui::Delete_account *ui;
};

#endif // DELETE_ACCOUNT_H
