#ifndef RESET_PASS_H
#define RESET_PASS_H

#include <QWidget>

namespace Ui {
class Reset_pass;
}

class Reset_pass : public QWidget
{
    Q_OBJECT

public:
    explicit Reset_pass(QWidget *parent = 0);
    ~Reset_pass();

private slots:
    void on_ResetButton_clicked();

private:
    Ui::Reset_pass *ui;
};

#endif // RESET_PASS_H
