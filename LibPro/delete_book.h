#ifndef DELETE_BOOK_H
#define DELETE_BOOK_H

#include <QWidget>

namespace Ui {
class Delete_book;
}

class Delete_book : public QWidget
{
    Q_OBJECT

public:
    explicit Delete_book(QWidget *parent = 0);
    ~Delete_book();

private:
    Ui::Delete_book *ui;
};

#endif // DELETE_BOOK_H
