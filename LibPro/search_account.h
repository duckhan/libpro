#ifndef SEARCH_ACCOUNT_H
#define SEARCH_ACCOUNT_H

#include <QWidget>

namespace Ui {
class Search_account;
}

class Search_account : public QWidget
{
    Q_OBJECT

public:
    explicit Search_account(QWidget *parent = 0);
    ~Search_account();

private:
    Ui::Search_account *ui;
};

#endif // SEARCH_ACCOUNT_H
