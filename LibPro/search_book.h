#ifndef SEARCH_BOOK_H
#define SEARCH_BOOK_H

#include <QWidget>

namespace Ui {
class Search_book;
}

class Search_book : public QWidget
{
    Q_OBJECT

public:
    explicit Search_book(QWidget *parent = 0);
    ~Search_book();

private slots:
    void on_BackButton_clicked();

    void on_HomeButton_clicked();

    void on_SearchlineEdit_returnPressed();

private:
    Ui::Search_book *ui;
};

#endif // SEARCH_BOOK_H
