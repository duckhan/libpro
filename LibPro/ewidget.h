#ifndef EWIDGET_H
#define EWIDGET_H
enum eWidget{
    WAdd_account,
    WAdd_book,
    WAdmin_profile,
    Wborrowed_book,
    WChange_pass,
    WDelete_account,
    WDelete_book,
    WHistory,
    WLibra_account,
    WLock_acount,
    WLogin,
    WMain_menu,
    WModify_account,
    WModify_book,
    WNotice,
    WPunish,
    WSearch,
    WSearch_account,
    WSearch_book,
    WTaken_book,
    WUnlock_account,
    WUser_profile
};

#endif // EWIDGET_H
