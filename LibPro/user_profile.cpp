#include "user_profile.h"
#include "ui_user_profile.h"
#include "navigationservice.h"
User_profile::User_profile(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::User_profile)
{
    ui->setupUi(this);
}

User_profile::~User_profile()
{
    delete ui;
}

void User_profile::on_NoticeButton_clicked()
{
    NavigationService::getInstance().navigate(eWidget::WNotice);
}
