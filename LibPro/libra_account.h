#ifndef LIBRA_ACCOUNT_H
#define LIBRA_ACCOUNT_H

#include <QWidget>

namespace Ui {
class Libra_account;
}

class Libra_account : public QWidget
{
    Q_OBJECT

public:
    explicit Libra_account(QWidget *parent = 0);
    ~Libra_account();

private:
    Ui::Libra_account *ui;
};

#endif // LIBRA_ACCOUNT_H
