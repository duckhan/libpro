#include "search_book.h"
#include "ui_search_book.h"
#include <QMessageBox>
#include "navigationservice.h"
#include "../LibProLib/Manager/bookmanager.h"
#include <QStandardItemModel>
Search_book::Search_book(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Search_book)
{
    ui->setupUi(this);
}

Search_book::~Search_book()
{
    delete ui;
}

void Search_book::on_BackButton_clicked()
{
    NavigationService::getInstance().goBack();
}

void Search_book::on_HomeButton_clicked()
{
    NavigationService::getInstance().goHome();
}

void Search_book::on_SearchlineEdit_returnPressed()
{

    QString title=ui->SearchlineEdit->text();
    QList<Book*> books=BookManager::getInstance().getBooksByTitle(title);
    QStandardItemModel* model=new QStandardItemModel(books.length(),6);
    model->setHorizontalHeaderItem(0,new QStandardItem("Book Id"));
    model->setHorizontalHeaderItem(1,new QStandardItem("Title"));
    model->setHorizontalHeaderItem(2,new QStandardItem("Author"));
    model->setHorizontalHeaderItem(3,new QStandardItem("Publisher"));
    model->setHorizontalHeaderItem(4,new QStandardItem("Available"));
    model->setHorizontalHeaderItem(5,new QStandardItem("Description"));
    QStandardItem* item;

    for(int i=0;i<books.length();i++)
    {
        Book* b=books.at(i);
        item=new QStandardItem(QString::number(b->getBookID()));
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,0,item);


        item=new QStandardItem(b->getTitle());
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,1,item);


        item=new QStandardItem(b->getAuthor());
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,2,item);


        item=new QStandardItem(b->getPublisher());
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,3,item);



        item=new QStandardItem(QString::number(b->getQuantity()));
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);
        model->setItem(i,4,item);

        item=new QStandardItem(b->getDecription());
        item->setTextAlignment(Qt::AlignCenter);
        item->setEditable(false);

        model->setItem(i,5,item);

    }
    ui->tableView->setModel(model);
    ui->tableView->resizeColumnsToContents();
    ui->tableView->horizontalHeader()->setSectionResizeMode(5,QHeaderView::Stretch);
}
