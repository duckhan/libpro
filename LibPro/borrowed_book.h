#ifndef BORROWED_BOOK_H
#define BORROWED_BOOK_H

#include <QWidget>

namespace Ui {
class Borrowed_book;
}

class Borrowed_book : public QWidget
{
    Q_OBJECT

public:
    explicit Borrowed_book(QWidget *parent = 0);
    ~Borrowed_book();

private:
    Ui::Borrowed_book *ui;
};

#endif // BORROWED_BOOK_H
